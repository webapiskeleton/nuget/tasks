﻿# WebApiSkeleton.Tasks

`WebApiSkeleton.Tasks` is a NuGet package that contains an implementation of distributed task queue persisted in `Redis`
database. InMemory bus or RabbitMQ can be used as
a [transport](./src/WebApiSkeleton.Tasks/MessageBrokerImplementation.cs)

## Usage

### Defining tasks and handlers

The task if defined by a class that is inherited
from [`BaseBackgroundTask`](./src/WebApiSkeleton.Tasks/Tasks/BaseBackgroundTask.cs) (
or [`PeriodicTask`](./src/WebApiSkeleton.Tasks/Tasks/PeriodicTask.cs)). `AdditionalExecuteAttemptsLeft` property, that
defines the number of attempts to complete a task after a failure, can be overriden to set the needed attempt count.

A handler for task must implement [`ITaskHandler`](./src/WebApiSkeleton.Tasks/TaskHandlers/ITaskHandler.cs) interface.
It can inject any dependencies needed from DI container as it is registered and resolved as transient.

### Modifying task state

Task state is modified by calling `OnStateChanged` action on the instance of `BaseBackgroundTask`. The argument of the
action is [`TaskState`](./src/WebApiSkeleton.Tasks/Models/TaskState.cs) object that contains following fields:

* `TaskId` - identifier of a task that current state is referring to. Initialized from a constructor;
* `StateId` - identifier of a state from [`TaskStates`](./src/WebApiSkeleton.Tasks/Models/TaskStates.cs) enum
  represented as integer;
* `PercentComplete`- percent of task completion to display the progress of a task. Cannot be greater then 100;
* `Comment` - optional description of provided state;
* `IsFinal` - if `true` then the state of provided task is not updated anymore.

After calling `OnStateChanged` action the `TaskState` object is sent to all servers using message broker (in
RabbitMQ `fanout` exchange is used). The received `TaskState` message is passed
to [`ITaskIntegrityService`](./src/WebApiSkeleton.Tasks/Integrity/ITaskIntegrityService.cs) implementation.

### Queue interaction

Queue interaction is done by
using [`IClientBackgroundTaskQueue`](./src/WebApiSkeleton.Tasks/Queue/Client/IClientBackgroundTaskQueue.cs):

* `EnqueueTask` method adds the new task to distributed queue to be handled by any server;
* `CancelTask` sends the [`CancellationRequest`](./src/WebApiSkeleton.Tasks/Models/CancellationRequest.cs) to all
  servers, meaning that the task is canceled by user and should not be handled anywhere. If task is already being
  handled on any server then it will run until handler calls `task.CancellationToken.ThrowIfCancellationRequested`
  method. If
  it doesn't then the task will be run until success or failure.

Queue can be also accessed by
using [`ITaskQueueStorage`](./src/WebApiSkeleton.Tasks/Integrity/Storage/ITaskQueueStorage.cs) methods to get tasks from
Redis.

### Registering the dependencies

Required dependencies are added to DI container by
using [`AddTaskQueue`](./src/WebApiSkeleton.Tasks/ServiceCollectionExtensions.cs) method on `IServiceCollection`. All
configuration is done by modifying [`TaskQueueConfiguration`](./src/WebApiSkeleton.Tasks/TaskQueueConfiguration.cs)
object.

The following settings are required and will throw if not specified or configured with errors:

* `RedisSettings` - contains the address of Redis database and a database number to use;
* [`ConsumerSettings`](./src/WebApiSkeleton.Tasks/Settings/ConsumerSettings.cs) - contains the **unique** server name
  that is used in message broker to identify available server correctly. Also contains `JsonSerializerSettings` that are
  added to DI and must have `TypeNameHandling` set to `All`;
* `TaskIntergrityImplementationType` - the type of a service that
  implements [`ITaskIntegrityService`](./src/WebApiSkeleton.Tasks/Integrity/ITaskIntegrityService.cs) that handles
  status changes of tasks in queue;
* `BrokerImplementation` - cannot be set directly, but one of methods that defines it must be
  called: `UseRabbitMQ`, `UseInMemoryBroker`;

Settings that have default values that can be overriden if needed:

* [`TaskStorageSettings`](./src/WebApiSkeleton.Tasks/Settings/TaskStorageSettings.cs) - contains names of task
  collections in Redis;
* [`TaskSemaphoreSettings`](./src/WebApiSkeleton.Tasks/Settings/TaskSemaphoreSettings.cs) - contains the name of
  semaphore, that is used to process tasks on this server, and maximum request (task) count for it. The name is
  recommended to be unique, otherwise multiple servers will use the same semaphore and maximum request count will be
  applied to multiple server instances simultaneously.

Tasks and handlers must also be added to DI container by using `AddTasksFromAssembly` method. Added assemblies will be
scanned for tasks and handlers. All handlers are added to DI as `Transient`.

NOTE:
you cannot use `AddTaskQueue` method more that 1 time as it will throw an exception.

## Example

Example projects can be found in [directory](./examples) with the same name.

* [`WebApiSkeleton.Tasks.Example.TasksLibrary`](./examples/WebApiSkeleton.Tasks.Example.TasksLibrary) defines task
  classes that are used across all servers;
* [`WebApiSkeleton.Tasks.Example.ConsumerServer`](./examples/WebApiSkeleton.Tasks.Example.ConsumerServer) is a Web API
  project that is used as one of servers to handle tasks distributed way. Can be run in multiple instances (server and
  semaphore names are generated randomly on every startup);
* [`WebApiSkeleton.Tasks.Example.BlazorClient`](./examples/WebApiSkeleton.Tasks.Example.BlazorClient) is a client with
  UI that also acts as a handle server. UI displays the state of all tasks across queue.

## Versioning

All projects are versioned using following format: `major.minor.patch`. Versioning rules for all projects:

- `patch` needs to be incremented when any minor change is made to the project, such as bugfixes or small
  project-specific features added
- `minor` needs to be incremented when new template-wide feature is implemented. In this case all of the projects must
  have the same version set
- `major` needs to be incremented when the `WebApiSkeleton` template has experienced significant changes, that need to
  upgrade all of the template packages. In this case all of the projects must
  have the same version set
