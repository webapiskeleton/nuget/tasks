﻿using MediatR;
using Microsoft.AspNetCore.SignalR;
using WebApiSkeleton.Tasks.Models;

namespace WebApiSkeleton.Tasks.Example.BlazorClient.Notifications;

public record TaskUpdatedNotification(TaskState NewState) : INotification;

public sealed class TaskUpdatedNotificationHandler : INotificationHandler<TaskUpdatedNotification>
{
    private readonly IHubContext<TaskNotificationHub> _hub;

    public TaskUpdatedNotificationHandler(IHubContext<TaskNotificationHub> hub)
    {
        _hub = hub;
    }

    public async Task Handle(TaskUpdatedNotification notification, CancellationToken cancellationToken)
    {
        await _hub.Clients.All.SendAsync("TaskUpdated", notification, cancellationToken);
    }
}