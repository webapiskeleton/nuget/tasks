using System.Reflection;
using Microsoft.AspNetCore.ResponseCompression;
using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Tasks;
using WebApiSkeleton.Tasks.Example.BlazorClient;
using WebApiSkeleton.Tasks.Example.BlazorClient.Components;
using WebApiSkeleton.Tasks.Example.BlazorClient.Tasks;
using WebApiSkeleton.Tasks.Example.TasksLibrary;
using WebApiSkeleton.Tasks.Settings;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorComponents()
    .AddInteractiveServerComponents();
builder.Services.AddTaskQueue(options =>
{
    options.RedisSettings = new RedisSettings
    {
        ConnectionMultiplexer = ConnectionMultiplexer.Connect(builder.Configuration.GetConnectionString("Redis")!),
        DatabaseNumber = 1
    };

    options.TaskSemaphoreSettings = new TaskSemaphoreSettings
    {
        SemaphoreName = builder.Configuration["SemaphoreSettings:SemaphoreName"]!,
        MaximumRequestCount = int.Parse(builder.Configuration["SemaphoreSettings:MaximumRequestCount"]!),
    };

    options.UseRabbitMQ(new RabbitMqSettings
    {
        Host = builder.Configuration["RabbitMqSettings:Host"]!,
        Port = ushort.Parse(builder.Configuration["RabbitMqSettings:Port"]!),
        Username = builder.Configuration["RabbitMqSettings:Username"]!,
        Password = builder.Configuration["RabbitMqSettings:Password"]!,
        VirtualHost = builder.Configuration["RabbitMqSettings:VirtualHost"]!,
    });

    options.TaskIntegrityImplementationType = typeof(TaskIntegrityService);

    options.ConsumerSettings = new ConsumerSettings
    {
        ServerName = builder.Configuration["ServerName"]!
    };

    options.AddTasksFromAssembly(typeof(TestTask).Assembly, Assembly.GetExecutingAssembly());
});

builder.Services.AddMediatR(config => { config.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()); });

builder.Services.AddResponseCompression(opts =>
{
    opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] { "application/octet-stream" });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error", createScopeForErrors: true);
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseResponseCompression();
app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseAntiforgery();

app.MapHub<TaskNotificationHub>("/task-hub");
app.MapRazorComponents<App>()
    .AddInteractiveServerRenderMode();

app.Run();