﻿using MediatR;
using WebApiSkeleton.Tasks.Example.BlazorClient.Notifications;
using WebApiSkeleton.Tasks.Integrity;
using WebApiSkeleton.Tasks.Models;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Example.BlazorClient.Tasks;

public sealed class TaskIntegrityService : ITaskIntegrityService
{
    private readonly ILogger<TaskIntegrityService> _logger;
    private readonly IPublisher _publisher;

    public TaskIntegrityService(ILogger<TaskIntegrityService> logger, IPublisher publisher)
    {
        _logger = logger;
        _publisher = publisher;
    }

    public async Task ChangeTaskStatusAsync(TaskState taskState)
    {
        if ((TaskStates)taskState.StateId == TaskStates.Active)
        {
            _logger.LogInformation("Received task: {TaskId} {TaskState} {TaskStateComment} {TaskStatePercentComplete}",
                taskState.TaskId, (TaskStates)taskState.StateId, taskState.Comment, taskState.PercentComplete);
        }

        var notification = new TaskUpdatedNotification(taskState);
        await _publisher.Publish(notification);
    }

    public async Task SaveTaskAsync(BaseBackgroundTask task)
    {
        await _publisher.Publish(new TaskUpdatedNotification(new TaskState(task.Id)
            { StateId = (int)TaskStates.InQueue }));
    }
}