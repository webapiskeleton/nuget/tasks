﻿using WebApiSkeleton.Tasks.Example.TasksLibrary;
using WebApiSkeleton.Tasks.Models;
using WebApiSkeleton.Tasks.Settings;
using WebApiSkeleton.Tasks.TaskHandlers;

namespace WebApiSkeleton.Tasks.Example.BlazorClient.Tasks;

public sealed class TestTaskHandler : ITaskHandler<TestTask>
{
    private readonly ConsumerSettings _consumerSettings;

    public TestTaskHandler(ConsumerSettings consumerSettings)
    {
        _consumerSettings = consumerSettings;
    }

    public async Task Handle(TestTask task)
    {
        task.OnStateChanged(new TaskState(task)
        {
            StateId = (int)TaskStates.Active,
            PercentComplete = 20,
            Comment = $"{_consumerSettings.ServerName}: Executing a task!"
        });
        await Task.Delay(3000);

        task.OnStateChanged(new TaskState(task)
        {
            StateId = (int)TaskStates.Active,
            PercentComplete = 60,
            Comment = $"{_consumerSettings.ServerName}: Executing a task longer that expected!"
        });

        await Task.Delay(2000);

        task.OnStateChanged(new TaskState(task)
        {
            StateId = (int)TaskStates.Completed,
            PercentComplete = 100,
            Comment = $"{_consumerSettings.ServerName}: Task completed!",
            IsFinal = true
        });
    }
}

public class TestPeriodicTaskHandler : ITaskHandler<TestPeriodicTask>
{
    private readonly ConsumerSettings _consumerSettings;

    public TestPeriodicTaskHandler(ConsumerSettings consumerSettings)
    {
        _consumerSettings = consumerSettings;
    }

    public async Task Handle(TestPeriodicTask task)
    {
        task.OnStateChanged(new TaskState(task)
        {
            StateId = (int)TaskStates.Active,
            PercentComplete = 20,
            Comment = $"{_consumerSettings.ServerName}: Executing a periodic task!"
        });
        await Task.Delay(3000);

        task.OnStateChanged(new TaskState(task)
        {
            StateId = (int)TaskStates.Active,
            PercentComplete = 60,
            Comment = $"{_consumerSettings.ServerName}: Executing a periodic task longer that expected!"
        });

        await Task.Delay(2000);

        task.OnStateChanged(new TaskState(task)
        {
            StateId = (int)TaskStates.Active,
            PercentComplete = 100,
            Comment = $"{_consumerSettings.ServerName}: Periodic task completed!",
        });
    }
}