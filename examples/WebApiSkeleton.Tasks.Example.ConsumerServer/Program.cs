using System.Reflection;
using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Tasks;
using WebApiSkeleton.Tasks.Example.ConsumerServer;
using WebApiSkeleton.Tasks.Example.TasksLibrary;
using WebApiSkeleton.Tasks.Settings;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddTaskQueue(options =>
{
    var serverGuid = Guid.NewGuid();
    options.RedisSettings = new RedisSettings
    {
        ConnectionMultiplexer = ConnectionMultiplexer.Connect(builder.Configuration.GetConnectionString("Redis")!),
        DatabaseNumber = 1
    };

    options.TaskSemaphoreSettings = new TaskSemaphoreSettings
    {
        SemaphoreName = builder.Configuration["SemaphoreSettings:SemaphoreName"]! + serverGuid,
        MaximumRequestCount = int.Parse(builder.Configuration["SemaphoreSettings:MaximumRequestCount"]!),
    };

    options.UseRabbitMQ(new RabbitMqSettings
    {
        Host = builder.Configuration["RabbitMqSettings:Host"]!,
        Port = ushort.Parse(builder.Configuration["RabbitMqSettings:Port"]!),
        Username = builder.Configuration["RabbitMqSettings:Username"]!,
        Password = builder.Configuration["RabbitMqSettings:Password"]!,
        VirtualHost = builder.Configuration["RabbitMqSettings:VirtualHost"]!,
    });

    options.TaskIntegrityImplementationType = typeof(TaskIntegrityService);

    options.ConsumerSettings = new ConsumerSettings
    {
        ServerName = builder.Configuration["ServerName"]! + serverGuid
    };

    options.AddTasksFromAssembly(typeof(TestTask).Assembly, Assembly.GetExecutingAssembly());
});
var app = builder.Build();

app.MapGet("/", () => "Hello World!");

app.Run();