﻿using WebApiSkeleton.Tasks.Integrity;
using WebApiSkeleton.Tasks.Models;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Example.ConsumerServer;

public class TaskIntegrityService : ITaskIntegrityService
{
    private readonly ILogger<TaskIntegrityService> _logger;

    public TaskIntegrityService(ILogger<TaskIntegrityService> logger)
    {
        _logger = logger;
    }

    public Task ChangeTaskStatusAsync(TaskState taskState)
    {
        _logger.LogInformation("{Id} - {State}", taskState.TaskId, (TaskStates)taskState.StateId);
        return Task.CompletedTask;
    }

    public Task SaveTaskAsync(BaseBackgroundTask task)
    {
        return Task.CompletedTask;
    }
}