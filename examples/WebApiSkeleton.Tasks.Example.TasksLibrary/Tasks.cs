﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Example.TasksLibrary;

public class TestTask : BaseBackgroundTask;

public class TestPeriodicTask : PeriodicTask
{
    public override TimeSpan Period => TimeSpan.FromSeconds(5);
}
