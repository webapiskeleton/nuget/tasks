﻿namespace WebApiSkeleton.Tasks.Constants;

internal static class TaskConstants
{
    internal const int DefaultAdditionalExecuteAttempts = 3;
}