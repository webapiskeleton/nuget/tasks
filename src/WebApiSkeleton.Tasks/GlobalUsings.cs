// Global using directives

global using MassTransit;
global using WebApiSkeleton.Tasks.Constants;
global using WebApiSkeleton.Tasks.Models;