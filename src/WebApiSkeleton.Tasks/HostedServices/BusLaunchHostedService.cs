﻿using Microsoft.Extensions.Hosting;

namespace WebApiSkeleton.Tasks.HostedServices;

internal sealed class BusLaunchHostedService : BackgroundService
{
    private readonly IBus _bus;

    public BusLaunchHostedService(IBus bus)
    {
        _bus = bus;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        return Task.CompletedTask;
    }
}