﻿using System.Reflection;
using Microsoft.Extensions.Logging;
using Quartz;
using WebApiSkeleton.DistributeLockUtilities.Services;
using WebApiSkeleton.Tasks.Integrity.Storage;
using WebApiSkeleton.Tasks.Queue.Client;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.HostedServices;

/// <summary>
/// Quartz background job of restoring deferred tasks from redis storage
/// </summary>
/// <remarks>Restoration action uses distributed lock to prevent concurrent restoration and double task execution</remarks>
[DisallowConcurrentExecution]
internal sealed class TaskRestorationBackgroundJob : IJob
{
    private const string TaskRestorationLockKey = "task-restoration-job";

    private readonly ILogger<TaskRestorationBackgroundJob> _logger;
    private readonly IDeferredTaskStorage _deferredTaskStorage;
    private readonly ITaskQueueStorage _taskQueueStorage;
    private readonly IClientBackgroundTaskQueue _clientQueue;
    private readonly ITaskStorageUtility _storageUtility;
    private readonly IDistributedLockService _distributedLockService;

    private readonly MethodInfo _clientQueueMethod;

    public TaskRestorationBackgroundJob(ILogger<TaskRestorationBackgroundJob> logger,
        IDeferredTaskStorage deferredTaskStorage,
        IClientBackgroundTaskQueue clientQueue,
        ITaskQueueStorage taskQueueStorage,
        ITaskStorageUtility storageUtility,
        IDistributedLockService distributedLockService)
    {
        _logger = logger;
        _deferredTaskStorage = deferredTaskStorage;
        _clientQueue = clientQueue;
        _taskQueueStorage = taskQueueStorage;
        _storageUtility = storageUtility;
        _distributedLockService = distributedLockService;
        _clientQueueMethod = _clientQueue
            .GetType()
            .GetMethod(nameof(ClientBackgroundTaskQueue.EnqueueTask), BindingFlags.Instance | BindingFlags.Public)!;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        var deferredTasks = _deferredTaskStorage.GetTasks();
        if (deferredTasks.Count == 0)
        {
            return;
        }

        await using var restorationLock = await _distributedLockService.AcquireLockAsync(TaskRestorationLockKey);
        if (restorationLock is null)
        {
            return;
        }

        await RestoreTasks(deferredTasks);
    }

    private async Task RestoreTasks(IReadOnlyDictionary<Guid, DeferredSign> tasks)
    {
        var tasksToRestore = new List<BaseBackgroundTask>();
        foreach (var (taskId, sign) in tasks)
        {
            // time has not come to run the task
            if (sign.NextRunTimeTicks.HasValue && DateTime.Now < new DateTime(sign.NextRunTimeTicks.Value))
                continue;
            var storedTask = _taskQueueStorage.GetTask(taskId);

            if (storedTask is null)
            {
                await _deferredTaskStorage.RemoveTaskAsync(taskId);
                continue;
            }

            if (_storageUtility.CheckTaskCancelled(taskId))
            {
                _logger.LogWarning("Task {TaskId} is cancelled and will not be restored", taskId);
                await _taskQueueStorage.RemoveTaskAsync(taskId);
                await _deferredTaskStorage.RemoveTaskAsync(taskId);
                continue;
            }

            tasksToRestore.Add(storedTask);
        }

        foreach (var task in tasksToRestore.OrderBy(x => x.AddedToQueueTime))
        {
            var sign = task.DeferredSign ?? tasks[task.Id];
            unchecked
            {
                sign.RepeatNumber++;
                sign.NextRunTimeTicks = DateTime.Now.Add(TimeSpan.FromMilliseconds(500)).Ticks;
            }

            task.DeferredSign = sign;
            task.IsRestored = true;
            await _taskQueueStorage.AddOrUpdateTaskAsync(task);
            await _deferredTaskStorage.RemoveTaskAsync(task.Id);
            await EnqueueTaskInternal(task);
            _logger.LogInformation("Restored task {TaskId} from storage", task.Id);
        }
    }

    private Task EnqueueTaskInternal(BaseBackgroundTask task)
    {
        var genericMethod = _clientQueueMethod.MakeGenericMethod(task.GetType());
        return (Task)genericMethod.Invoke(_clientQueue, new object[] { task })!;
    }
}