﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity;

/// <summary>
/// Used to manage the changing of task state. Called in process of task handling
/// </summary>
public interface ITaskIntegrityService
{
    /// <summary>
    /// Handle task status change
    /// </summary>
    Task ChangeTaskStatusAsync(TaskState taskState);

    /// <summary>
    /// Save task
    /// </summary>
    Task SaveTaskAsync(BaseBackgroundTask task);
}