﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity.Storage;

internal interface IDeferredTaskStorage
{
    public Task AddTaskAsync(BaseBackgroundTask task);
    public Task RemoveTaskAsync(Guid taskId);
    public IReadOnlyDictionary<Guid, DeferredSign> GetTasks();
}