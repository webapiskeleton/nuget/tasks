﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity.Storage;

public interface ITaskQueueStorage
{
    public Task AddOrUpdateTaskAsync(BaseBackgroundTask task, bool isActive = false);
    internal Task RemoveTaskAsync(Guid taskId);
    public BaseBackgroundTask? GetTask(Guid taskId, Models.TaskQueueStatus queueStatus = Models.TaskQueueStatus.All);
    public IReadOnlyCollection<BaseBackgroundTask> GetTasks(Models.TaskQueueStatus queueStatus = Models.TaskQueueStatus.All);
}