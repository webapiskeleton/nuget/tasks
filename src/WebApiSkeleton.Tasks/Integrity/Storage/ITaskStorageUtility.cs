﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity.Storage;

internal interface ITaskStorageUtility
{
    public Task AddTaskToFinishedAsync(Guid taskId);
    public bool CheckTaskFinished(Guid taskId);
    public Task AddTaskToCancelledAsync(Guid taskId);
    public bool CheckTaskCancelled(Guid taskId);

    public string SerializeForRedis(BaseBackgroundTask task);
    public BaseBackgroundTask DeserializeFromRedis(string value);
}