﻿using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Tasks.Settings;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity.Storage.Impl;

internal sealed class RedisDeferredTaskStorage : IDeferredTaskStorage
{
    private readonly IDatabase _database;
    private readonly TaskStorageSettings _storageSettings;


    public RedisDeferredTaskStorage(RedisSettings redisConnectionSettings,
        TaskStorageSettings storageSettings)
    {
        _storageSettings = storageSettings;
        _database = redisConnectionSettings.ConnectionMultiplexer
            .GetDatabase(redisConnectionSettings.DatabaseNumber);
    }

    public async Task AddTaskAsync(BaseBackgroundTask task)
    {
        await _database.HashSetAsync(_storageSettings.DeferredTasksTableName, task.Id.ToString(),
            task.DeferredSign?.ToString() ?? "0");
    }

    public async Task RemoveTaskAsync(Guid taskId)
    {
        await _database.HashDeleteAsync(_storageSettings.DeferredTasksTableName, taskId.ToString());
    }

    public IReadOnlyDictionary<Guid, DeferredSign> GetTasks()
    {
        return _database.HashGetAll(_storageSettings.DeferredTasksTableName)
            .ToDictionary(x => Guid.Parse(x.Name), x => DeferredSign.Parse(x.Value));
    }
}