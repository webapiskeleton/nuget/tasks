﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Tasks.Settings;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity.Storage.Impl;

internal sealed class RedisTaskQueueStorage : ITaskQueueStorage
{
    private readonly IDatabase _database;
    private readonly TaskStorageSettings _storageSettings;
    private readonly ILogger<RedisTaskQueueStorage> _logger;
    private readonly ITaskStorageUtility _storageUtility;

    public RedisTaskQueueStorage(RedisSettings redisConnectionSettings,
        TaskStorageSettings storageSettings,
        ILogger<RedisTaskQueueStorage> logger, 
        ITaskStorageUtility storageUtility)
    {
        _storageSettings = storageSettings;
        _logger = logger;
        _storageUtility = storageUtility;
        _database = redisConnectionSettings.ConnectionMultiplexer
            .GetDatabase(redisConnectionSettings.DatabaseNumber);
    }

    public async Task AddOrUpdateTaskAsync(BaseBackgroundTask task, bool isActive = false)
    {
        var serializedTask = _storageUtility.SerializeForRedis(task);
        await _database.HashSetAsync(
            isActive ? _storageSettings.ExecutingTasksTableName : _storageSettings.TaskQueueTableName,
            task.Id.ToString(), serializedTask);
        if (isActive)
            await _database.HashDeleteAsync(_storageSettings.TaskQueueTableName, task.Id.ToString());
    }

    public async Task RemoveTaskAsync(Guid taskId)
    {
        await _database.HashDeleteAsync(_storageSettings.TaskQueueTableName, taskId.ToString());
        await _database.HashDeleteAsync(_storageSettings.ExecutingTasksTableName, taskId.ToString());
    }

    public BaseBackgroundTask? GetTask(Guid taskId, TaskQueueStatus queueStatus = TaskQueueStatus.All)
    {
        string table;
        switch (queueStatus)
        {
            case TaskQueueStatus.Active:
                table = _storageSettings.ExecutingTasksTableName;
                break;
            case TaskQueueStatus.Queued:
                table = _storageSettings.TaskQueueTableName;
                break;
            case TaskQueueStatus.All:
                return GetTask(taskId, TaskQueueStatus.Active) ??
                       GetTask(taskId, TaskQueueStatus.Queued);
            default:
                throw new ArgumentOutOfRangeException(nameof(queueStatus), queueStatus, null);
        }

        var redisTask = _database.HashGet(table, taskId.ToString());
        return !redisTask.HasValue ? default : _storageUtility.DeserializeFromRedis(redisTask);
    }

    public IReadOnlyCollection<BaseBackgroundTask> GetTasks(TaskQueueStatus queueStatus = TaskQueueStatus.All)
    {
        string table;
        switch (queueStatus)
        {
            case TaskQueueStatus.Active:
                table = _storageSettings.ExecutingTasksTableName;
                break;
            case TaskQueueStatus.Queued:
                table = _storageSettings.TaskQueueTableName;
                break;
            case TaskQueueStatus.All:
                var active = GetTasks(TaskQueueStatus.Active).ToList();
                active.AddRange(GetTasks(TaskQueueStatus.Queued));
                return active.AsReadOnly();
            default:
                throw new ArgumentOutOfRangeException(nameof(queueStatus), queueStatus, null);
        }

        var taskIds = _database.HashGetAll(table).Select(x => x.Value);
        var redisValues = _database.HashGet(table, taskIds.ToArray());

        var result = new List<BaseBackgroundTask>();
        foreach (var value in redisValues)
        {
            try
            {
                if (!value.HasValue)
                    continue;

                var task = _storageUtility.DeserializeFromRedis(value) ??
                           throw new JsonException("Не удалось десериализовать задачу из Redis");
                result.Add(task);
            }
            catch (JsonException exception)
            {
                _logger.LogWarning("Ошибка десериализации задачи из Redis при получении всех задач\n{Exception}",
                    exception);
            }
        }

        return result.AsReadOnly();
    }
}