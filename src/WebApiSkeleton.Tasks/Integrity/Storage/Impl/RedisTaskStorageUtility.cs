﻿using Newtonsoft.Json;
using StackExchange.Redis;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Tasks.Settings;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity.Storage.Impl;

internal class RedisTaskStorageUtility : ITaskStorageUtility
{
    private readonly IDatabase _database;
    private readonly TaskStorageSettings _storageSettings;
    private readonly JsonSerializerSettings _serializerSettings;

    public RedisTaskStorageUtility(RedisSettings redisConnectionSettings,
        TaskStorageSettings storageSettings, 
        JsonSerializerSettings serializerSettings)
    {
        _storageSettings = storageSettings;
        _serializerSettings = serializerSettings;
        _database = redisConnectionSettings.ConnectionMultiplexer
            .GetDatabase(redisConnectionSettings.DatabaseNumber);
    }

    public async Task AddTaskToFinishedAsync(Guid taskId)
    {
        await _database.HashSetAsync(_storageSettings.FinishedTasksTableName, taskId.ToString(), true);
    }

    public bool CheckTaskFinished(Guid taskId)
    {
        return _database.HashExists(_storageSettings.FinishedTasksTableName, taskId.ToString());
    }

    public async Task AddTaskToCancelledAsync(Guid taskId)
    {
        await _database.HashSetAsync(_storageSettings.CancelledTasksTableName, taskId.ToString(), true);
    }

    public bool CheckTaskCancelled(Guid taskId)
    {
        return _database.HashExists(_storageSettings.CancelledTasksTableName, taskId.ToString());
    }

    public string SerializeForRedis(BaseBackgroundTask task)
    {
        return JsonConvert.SerializeObject(task, _serializerSettings);
    }

    public BaseBackgroundTask DeserializeFromRedis(string value)
    {
        return JsonConvert.DeserializeObject<BaseBackgroundTask>(value, _serializerSettings)!;
    }
}