﻿using WebApiSkeleton.Tasks.Queue.Server;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Integrity.TaskFlowControl;

/// <summary>
/// Consumer for task cancellation request
/// </summary>
internal sealed class CancellationRequestConsumer : IConsumer<CancellationRequest>
{
    private readonly IServerBackgroundTaskQueue<BaseBackgroundTask> _serverQueue;

    public CancellationRequestConsumer(IServerBackgroundTaskQueue<BaseBackgroundTask> serverQueue)
    {
        _serverQueue = serverQueue;
    }

    public async Task Consume(ConsumeContext<CancellationRequest> context)
    {
        await _serverQueue.CancelTask(context.Message.TaskId);
    }
}