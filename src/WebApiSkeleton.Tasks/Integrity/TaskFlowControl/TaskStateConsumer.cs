﻿namespace WebApiSkeleton.Tasks.Integrity.TaskFlowControl;

/// <summary>
/// Consumer for handling task state changes
/// </summary>
internal sealed class TaskStateConsumer : IConsumer<TaskState>
{
    private readonly ITaskIntegrityService _integrityService;

    public TaskStateConsumer(ITaskIntegrityService integrityService)
    {
        _integrityService = integrityService;
    }

    public async Task Consume(ConsumeContext<TaskState> context)
    {
        await _integrityService.ChangeTaskStatusAsync(context.Message);
    }
}