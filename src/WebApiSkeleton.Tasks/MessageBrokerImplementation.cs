﻿namespace WebApiSkeleton.Tasks;

internal enum MessageBrokerImplementation
{
    RabbitMq = 1,
    InMemory = 2
}