﻿namespace WebApiSkeleton.Tasks.Models;

/// <summary>
/// Request to cancel the task in queue. Will cancel the running task only if handler uses <see cref="CancellationToken.ThrowIfCancellationRequested"/> method
/// </summary>
public sealed class CancellationRequest
{
    public Guid TaskId { get; set; }
}