﻿using Newtonsoft.Json;

namespace WebApiSkeleton.Tasks.Models;

/// <summary>
/// Defines the deferment parameters of tasks that cannot be executed at the moment 
/// </summary>
public sealed class DeferredSign
{
    /// <summary>
    /// The current attempt to execute
    /// </summary>
    public int RepeatNumber { get; set; }

    public long? NextRunTimeTicks { get; set; }

    [JsonConstructor]
    public DeferredSign(int repeatNumber, long? nextRunTime)
    {
        RepeatNumber = repeatNumber;
        NextRunTimeTicks = nextRunTime;
    }

    public DeferredSign(int repeatNumber)
    {
        RepeatNumber = repeatNumber;
    }

    public static DeferredSign Parse(string value)
    {
        var arr = value.Split(';');
        switch (arr.Length)
        {
            case 2:
            {
                var item1 = int.Parse(arr[0]);
                var item2 = long.Parse(arr[1]);
                return new DeferredSign(item1, item2);
            }
            case 1:
            {
                var item1 = int.Parse(arr[0]);
                return new DeferredSign(item1);
            }
            default:
                throw new InvalidOperationException($"Не удалось преобразовать строку {value}" +
                                                    $" в экземпляр {nameof(DeferredSign)}");
        }
    }

    public override string ToString()
    {
        return $"{RepeatNumber}{(NextRunTimeTicks.HasValue ? $";{NextRunTimeTicks.Value}" : string.Empty)}";
    }
}