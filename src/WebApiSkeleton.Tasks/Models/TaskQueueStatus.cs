﻿namespace WebApiSkeleton.Tasks.Models;

/// <summary>
/// Task statuses to handle storage so enum is internal
/// </summary>
public enum TaskQueueStatus
{
    Queued = 1,
    Active = 2,
    All = Queued | Active
}