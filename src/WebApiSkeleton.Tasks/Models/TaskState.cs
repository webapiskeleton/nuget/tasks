﻿using Newtonsoft.Json;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Models;

/// <summary>
/// Shows the state of task
/// </summary>
public sealed class TaskState
{
    public Guid TaskId { get; }

    /// <summary>
    /// State identifier from <see cref="TaskStates"/>
    /// </summary>
    public int StateId { get; set; }
    
    /// <summary>
    /// If current state is final, the task state cannot be ever changed again
    /// </summary>
    public bool IsFinal { get; set; }

    private uint _percentComplete;

    public uint PercentComplete
    {
        get => _percentComplete;
        set => _percentComplete = value > 100 ? 100 : value;
    }

    public string? Comment { get; set; }

    [JsonConstructor]
    public TaskState(Guid taskId)
    {
        TaskId = taskId;
    }

    public TaskState(BaseBackgroundTask task)
    {
        TaskId = task.Id;
    }
}