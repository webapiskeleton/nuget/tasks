namespace WebApiSkeleton.Tasks.Models;

/// <summary>
/// Possible task state codes
/// </summary>
public enum TaskStates
{
    /// <summary>
    /// Waiting for execution in queue
    /// </summary>
    InQueue = 1,
    
    /// <summary>
    /// Currently executing
    /// </summary>
    Active = 2,
    
    /// <summary>
    /// Completed with success
    /// </summary>
    Completed = 3,
    
    /// <summary>
    /// FOR PERIODIC TASKS: Delayed and waiting for execution when time comes
    /// </summary>
    Delayed = 4,
    
    /// <summary>
    /// Completed with failure
    /// </summary>
    Failed = 5,
    
    /// <summary>
    /// Cancelled by the user
    /// </summary>
    Cancelled = 6
}