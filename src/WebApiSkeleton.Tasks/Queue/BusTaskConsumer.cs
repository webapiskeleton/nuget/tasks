﻿using Microsoft.Extensions.Logging;
using WebApiSkeleton.Tasks.Queue.Server;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Queue;

/// <summary>
/// Masstransit consumer that receives tasks from message broker and passes them to <see cref="IServerBackgroundTaskQueue{TBaseTask}"/>
/// </summary>
/// <typeparam name="TTask"></typeparam>
internal sealed class BusTaskConsumer<TTask> : IConsumer<TTask> where TTask : BaseBackgroundTask
{
    private readonly ILogger<BusTaskConsumer<TTask>> _logger;
    private readonly IServerBackgroundTaskQueue<BaseBackgroundTask> _serverQueue;

    public BusTaskConsumer(IServerBackgroundTaskQueue<BaseBackgroundTask> serverQueue, ILogger<BusTaskConsumer<TTask>> logger)
    {
        _serverQueue = serverQueue;
        _logger = logger;
    }

    public async Task Consume(ConsumeContext<TTask> context)
    {
        await _serverQueue.ProcessReceivedTask(context.Message);
    }
}