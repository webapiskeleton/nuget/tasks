﻿using Microsoft.Extensions.Logging;
using WebApiSkeleton.Tasks.Integrity;
using WebApiSkeleton.Tasks.Integrity.Storage;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Queue.Client;

/// <inheritdoc/>
internal sealed class ClientBackgroundTaskQueue : IClientBackgroundTaskQueue
{
    private readonly IBus _bus;
    private readonly ITaskQueueStorage _taskQueueStorage;
    private readonly ILogger<ClientBackgroundTaskQueue> _logger;
    private readonly ITaskIntegrityService _integrityService;
    private readonly ITaskStorageUtility _storageUtility;

    public ClientBackgroundTaskQueue(IBus bus,
        ITaskQueueStorage taskQueueStorage,
        ILogger<ClientBackgroundTaskQueue> logger,
        ITaskIntegrityService integrityService,
        ITaskStorageUtility storageUtility,
        IServiceProvider serviceProvider)
    {
        _bus = bus;
        _taskQueueStorage = taskQueueStorage;
        _logger = logger;
        _integrityService = integrityService;
        _storageUtility = storageUtility;
    }

    /// <inheritdoc/>
    public async Task EnqueueTask<TTask>(TTask task) where TTask : BaseBackgroundTask
    {
        if (!task.IsRestored)
        {
            task.Id = Guid.NewGuid();
            task.AddedToQueueTime = DateTime.Now;
        }

        var state = new TaskState(task.Id)
        {
            StateId = (int)TaskStates.InQueue
        };
        await _bus.Publish(state);

        await _taskQueueStorage.AddOrUpdateTaskAsync(task);
        _logger.LogInformation("Task {TaskId} published", task.Id);
        await _integrityService.SaveTaskAsync(task);
        await _bus.Publish(task);
    }

    /// <inheritdoc/>
    public async Task CancelTask(Guid taskId)
    {
        var request = new CancellationRequest
        {
            TaskId = taskId
        };
        await _storageUtility.AddTaskToCancelledAsync(taskId);
        await _bus.Publish(request);
    }
}