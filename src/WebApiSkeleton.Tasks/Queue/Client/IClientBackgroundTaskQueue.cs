﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Queue.Client;

/// <summary>
/// Used to send tasks and <see cref="CancellationRequest"/> to the task queue. Does not contain execution logic and just guarantees that task will be added to queue
/// </summary>
public interface IClientBackgroundTaskQueue
{
    /// <summary>
    /// Add task to the queue
    /// </summary>
    public Task EnqueueTask<TTask>(TTask task) where TTask : BaseBackgroundTask;
    
    /// <summary>
    /// Cancel task that is waiting for execution in queue
    /// </summary>
    public Task CancelTask(Guid taskId);
}