﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Queue.Server;

/// <summary>
/// Contains task handle logic and all checks before execution. Tasks are received from the message broker
/// </summary>
internal interface IServerBackgroundTaskQueue<in TBaseTask> where TBaseTask : BaseBackgroundTask
{
    public Task<bool> ProcessReceivedTask<TTask>(TTask task) where TTask : TBaseTask;

    /// <summary>
    /// Cancel task execution on this server
    /// </summary>
    public Task CancelTask(Guid taskId);
}