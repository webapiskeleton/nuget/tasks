﻿using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebApiSkeleton.DistributeLockUtilities.Services;
using WebApiSkeleton.Tasks.Integrity;
using WebApiSkeleton.Tasks.Integrity.Storage;
using WebApiSkeleton.Tasks.Settings;
using WebApiSkeleton.Tasks.TaskHandlers;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.Queue.Server;

/// <inheritdoc/>
internal sealed class ServerBackgroundTaskQueue<TBaseTask> : IServerBackgroundTaskQueue<TBaseTask>
    where TBaseTask : BaseBackgroundTask
{
    private readonly ConcurrentDictionary<Guid, CancellationTokenSource> _cancellationTokens;
    private readonly ConcurrentDictionary<Guid, TaskState> _taskStatusCache;

    private readonly TaskSemaphoreSettings _semaphoreSettings;
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<ServerBackgroundTaskQueue<TBaseTask>> _logger;
    private readonly IDeferredTaskStorage _deferredTaskStorage;
    private readonly ITaskQueueStorage _taskQueueStorage;
    private readonly ITaskStorageUtility _storageUtility;
    private readonly IBus _bus;
    private readonly ITaskIntegrityService _integrityService;
    private readonly IDistributedLockService _distributedLockService;

    public ServerBackgroundTaskQueue(TaskSemaphoreSettings semaphoreSettings,
        ILogger<ServerBackgroundTaskQueue<TBaseTask>> logger,
        IServiceProvider serviceProvider,
        IDeferredTaskStorage deferredTaskStorage,
        ITaskQueueStorage taskQueueStorage,
        ITaskStorageUtility storageUtility,
        IBus bus,
        ITaskIntegrityService integrityService,
        IDistributedLockService distributedLockService)
    {
        _cancellationTokens = new ConcurrentDictionary<Guid, CancellationTokenSource>();
        _taskStatusCache = new ConcurrentDictionary<Guid, TaskState>();
        _semaphoreSettings = semaphoreSettings;
        _logger = logger;
        _serviceProvider = serviceProvider;
        _deferredTaskStorage = deferredTaskStorage;
        _taskQueueStorage = taskQueueStorage;
        _storageUtility = storageUtility;
        _bus = bus;
        _integrityService = integrityService;
        _distributedLockService = distributedLockService;
    }

    public async Task<bool> ProcessReceivedTask<TTask>(TTask task) where TTask : TBaseTask
    {
        await using var semaphoreLock =
            await _distributedLockService.AcquireSemaphoreAsync(_semaphoreSettings);
        if (semaphoreLock is null)
        {
            _logger.LogInformation("Could not get a semaphore lock for task {Id}, trying to requeue", task.Id);
            await RequeueTask(task);
            return false;
        }

        if (_storageUtility.CheckTaskCancelled(task.Id))
        {
            await _integrityService.ChangeTaskStatusAsync(new TaskState(task.Id)
            {
                StateId = (int)TaskStates.Cancelled, IsFinal = true
            });
            _logger.LogWarning("Task {TaskId} was cancelled before execution on server", task.Id);
            await ReleaseTaskResources(task);
            return true;
        }

        await using var taskLock = await _distributedLockService.AcquireLockAsync(task.Id.ToString());
        if (taskLock is null)
        {
            _logger.LogWarning("Task {TaskId} is already being processed by one of the servers", task.Id);
            await ReleaseTaskResources(task);
            return true;
        }

        task.OnStateChanged = OnStateChanged;
        if (!await StartTask(task))
        {
            return true;
        }

        repeat:
        var needRepeat = false;
        _logger.LogInformation("Task {TaskId} starts executing", task.Id);
        OnStateChanged(new TaskState(task.Id)
        {
            StateId = (int)TaskStates.Active,
            Comment = "Task executing started"
        });
        try
        {
            await HandleTask(task);
            _logger.LogInformation("Task {TaskId} successfully completed", task.Id);

            if (task is PeriodicTask pt)
            {
                var deferredSign = task.DeferredSign ?? new DeferredSign(0);
                deferredSign.RepeatNumber++;
                deferredSign.NextRunTimeTicks = DateTime.Now.Add(pt.Period).Ticks;
                task.DeferredSign = deferredSign;

                if (pt.MaximumCompletions.HasValue && deferredSign.RepeatNumber >= pt.MaximumCompletions)
                {
                    OnStateChanged(new TaskState(task.Id)
                    {
                        StateId = (int)TaskStates.Completed,
                        IsFinal = true
                    });
                    return true;
                }

                OnStateChanged(new TaskState(task.Id)
                {
                    StateId = (int)TaskStates.Delayed
                });
                await RequeueTask(task);
                needRepeat = true;
                return true;
            }

            OnStateChanged(new TaskState(task.Id)
            {
                IsFinal = true,
                StateId = (int)TaskStates.Completed,
                PercentComplete = 100,
                Comment = "Task successfully completed"
            });
        }
        catch (OperationCanceledException)
        {
            task.OnStateChanged(new TaskState(task.Id)
            {
                StateId = (int)TaskStates.Cancelled,
                IsFinal = true,
                Comment = "Task was cancelled"
            });
            await _storageUtility.AddTaskToCancelledAsync(task.Id);
            _logger.LogInformation("Task {TaskId} was cancelled", task.Id);
        }
        catch (Exception e)
        {
            _logger.LogError("Error while executing task {TaskId}\n{Exception}", task.Id, e);

            if (!NeedRepeatTask(task, ref needRepeat))
            {
                OnStateChanged(new TaskState(task.Id)
                {
                    StateId = (int)TaskStates.Failed,
                    IsFinal = true,
                    Comment = "Error while executing a task"
                });
                throw;
            }
        }
        finally
        {
            if (!needRepeat)
            {
                await ReleaseTaskResources(task);
            }
        }

        if (needRepeat)
            goto repeat;

        return true;
    }

    /// <inheritdoc/>
    public Task CancelTask(Guid taskId)
    {
        if (_cancellationTokens.TryGetValue(taskId, out var tokenSource))
        {
            _logger.LogWarning("Cancellation of task {TaskId} was requested", taskId);
            tokenSource.Cancel();
        }

        OnStateChanged(new TaskState(taskId)
        {
            StateId = (int)TaskStates.Cancelled,
            IsFinal = true
        });
        _cancellationTokens.TryRemove(taskId, out _);
        return Task.CompletedTask;
    }

    private void OnStateChanged(TaskState state)
    {
        if (_storageUtility.CheckTaskFinished(state.TaskId))
            return;

        if (_taskStatusCache.TryGetValue(state.TaskId, out var cachedState))
        {
            if (cachedState.IsFinal)
            {
                return;
            }
        }

        var status = _taskStatusCache.AddOrUpdate(state.TaskId, state, (_, _) => state);
        _bus.Publish(status);
    }

    private async Task<bool> StartTask(BaseBackgroundTask task)
    {
        if (_storageUtility.CheckTaskFinished(task.Id))
        {
            _logger.LogWarning("Task {TaskId} was already been completed and will not be added to queue", task.Id);
            await ReleaseTaskResources(task);
            return false;
        }

        AddOrUpdateCancellationToken(task);
        task.StartedTime = DateTime.Now;
        await _taskQueueStorage.AddOrUpdateTaskAsync(task, true);
        return true;
    }

    private void AddOrUpdateCancellationToken(BaseBackgroundTask task)
    {
        _cancellationTokens.AddOrUpdate(task.Id, _ =>
        {
            var tokenSource = new CancellationTokenSource();
            task.CancellationToken = tokenSource.Token;
            return tokenSource;
        }, (_, token) =>
        {
            if (!token.IsCancellationRequested) return token;
            var tokenSource = new CancellationTokenSource();
            task.CancellationToken = tokenSource.Token;
            return tokenSource;
        });
    }

    private bool NeedRepeatTask(BaseBackgroundTask task, ref bool needRepeat)
    {
        var processingTask = _taskQueueStorage.GetTask(task.Id, TaskQueueStatus.Active);

        if (processingTask is not { AdditionalExecuteAttemptsLeft: > 0 }) return false;

        task.Repeat();
        AddOrUpdateCancellationToken(task);
        _taskQueueStorage.AddOrUpdateTaskAsync(task, true);
        _logger.LogWarning(
            "Trying to repeat a task {TaskId}\nAttempt number: {AttemptNumber}\nAttempts left: {AttemptsLeft}",
            task.Id, task.AttemptNumber, task.AdditionalExecuteAttemptsLeft);

        needRepeat = true;
        return true;
    }

    private async Task ReleaseTaskResources(BaseBackgroundTask task)
    {
        await _taskQueueStorage.RemoveTaskAsync(task.Id);
        _taskStatusCache.TryRemove(task.Id, out _);
        _cancellationTokens.TryRemove(task.Id, out _);
        await _storageUtility.AddTaskToFinishedAsync(task.Id);
    }

    private async Task HandleTask<TTask>(TTask task) where TTask : BaseBackgroundTask
    {
        var handler = _serviceProvider.GetRequiredService<ITaskHandler<TTask>>();
        await handler.Handle(task);
    }

    private async Task RequeueTask(TBaseTask task)
    {
        task.IsDeferred = true;
        await _deferredTaskStorage.AddTaskAsync(task);
        await _taskQueueStorage.AddOrUpdateTaskAsync(task);
    }
}