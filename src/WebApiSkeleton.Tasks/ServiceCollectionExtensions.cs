﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Quartz;
using WebApiSkeleton.DistributeLockUtilities;
using WebApiSkeleton.Tasks.HostedServices;
using WebApiSkeleton.Tasks.Integrity;
using WebApiSkeleton.Tasks.Integrity.Storage;
using WebApiSkeleton.Tasks.Integrity.Storage.Impl;
using WebApiSkeleton.Tasks.Integrity.TaskFlowControl;
using WebApiSkeleton.Tasks.Queue;
using WebApiSkeleton.Tasks.Queue.Client;
using WebApiSkeleton.Tasks.Queue.Server;
using WebApiSkeleton.Tasks.TaskHandlers;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Add task queue using <see cref="TaskQueueConfiguration"/>
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/></param>
    /// <param name="taskQueueConfigurationCallback">Action to configure <see cref="TaskQueueConfiguration"/></param>
    /// <exception cref="ApplicationException">task queue is added two or more times</exception>
    public static IServiceCollection AddTaskQueue(this IServiceCollection services,
        Action<TaskQueueConfiguration> taskQueueConfigurationCallback)
    {
        if (services.Any(x => x.ServiceType == typeof(IBus)))
        {
            throw new ApplicationException("Task queue cannot be added twice");
        }

        var configuration = new TaskQueueConfiguration();
        taskQueueConfigurationCallback.Invoke(configuration);
        configuration.ValidateConfiguration();

        return services
            .AddBus(configuration)
            .AddConfiguration(configuration)
            .AddQueues()
            .AddTaskStateStorages()
            .AddQuartzBackgroundServices()
            .AddHostedService<BusLaunchHostedService>();
    }

    private static IServiceCollection AddConfiguration(this IServiceCollection services,
        TaskQueueConfiguration configuration)
    {        
        services.TryAddRedisDistributedLockService();
        services.TryAddSingleton(configuration.ConsumerSettings!.JsonSerializerSettings);
        services.TryAddSingleton(configuration.ConsumerSettings);
        services.TryAddSingleton(configuration.TaskStorageSettings);
        services.TryAddSingleton(configuration.TaskSemaphoreSettings);
        services.TryAddSingleton(typeof(ITaskIntegrityService), configuration.TaskIntegrityImplementationType!);
        services.TryAddSingleton(configuration.RedisSettings!);
        return services;
    }

    private static IServiceCollection AddQuartzBackgroundServices(this IServiceCollection services)
    {
        return services.AddQuartz(options =>
            {
                var key = JobKey.Create(nameof(TaskRestorationBackgroundJob));
                options.AddJob<TaskRestorationBackgroundJob>(key)
                    .AddTrigger(trigger =>
                    {
                        trigger.ForJob(key)
                            .WithSimpleSchedule(schedule =>
                                schedule.WithInterval(TimeSpan.FromMilliseconds(500))
                                    .RepeatForever());
                    });
            })
            .AddQuartzHostedService();
    }

    private static IServiceCollection AddTaskStateStorages(this IServiceCollection services)
    {
        return services.AddSingleton<ITaskQueueStorage, RedisTaskQueueStorage>()
            .AddSingleton<IDeferredTaskStorage, RedisDeferredTaskStorage>()
            .AddSingleton<ITaskStorageUtility, RedisTaskStorageUtility>();
    }

    private static IServiceCollection AddQueues(this IServiceCollection services)
    {
        return services
            .AddSingleton<IServerBackgroundTaskQueue<BaseBackgroundTask>,
                ServerBackgroundTaskQueue<BaseBackgroundTask>>()
            .AddSingleton<IClientBackgroundTaskQueue, ClientBackgroundTaskQueue>();
    }

    private static IServiceCollection AddBus(this IServiceCollection services, TaskQueueConfiguration configuration)
    {
        return services.AddMassTransit(options =>
        {
            AddTaskConsumers(services, configuration, options);

            AddServiceConsumers(options);

            AddBroker(configuration, options);
        });
    }

    private static void AddServiceConsumers(IServiceCollection services)
    {
        services.AddTransient<TaskStateConsumer>();
        services.AddTransient<CancellationRequestConsumer>();
    }

    private static void AddTaskConsumers(IServiceCollection services, TaskQueueConfiguration configuration,
        IBusRegistrationConfigurator options)
    {
        var busConsumerType = typeof(BusTaskConsumer<>);
        var tasks = configuration.TaskAssemblies
            .Distinct()
            .SelectMany(assembly => assembly.GetTypes()
                .Where(type => !type.IsAbstract
                               && type.IsAssignableTo(typeof(BaseBackgroundTask))
                )
            );
        var taskHandlers = configuration.TaskAssemblies
            .Distinct()
            .SelectMany(x => x.GetTypes()
                .Where(z =>
                    !z.IsAbstract && z.GetInterfaces().Any(i =>
                        i.IsGenericType && i.GetGenericTypeDefinition() == typeof(ITaskHandler<>))))
            .ToArray();
        foreach (var task in tasks.Distinct())
        {
            var foundHandlers = taskHandlers
                .Where(x => x.GetInterfaces()
                    .FirstOrDefault(i => i.GetGenericTypeDefinition() == typeof(ITaskHandler<>) &&
                                i.GetGenericArguments().First() == task)?.GetGenericArguments().First() == task)
                .ToArray();

            if (!foundHandlers.Any())
            {
                throw new ApplicationException($"No handlers found for task {task.Name}");
            }

            if (foundHandlers.Length > 1)
            {
                throw new ApplicationException($"Cannot have multiple handlers for a task {task.Name}");
            }

            services.AddTransient(typeof(ITaskHandler<>).MakeGenericType(task), foundHandlers.First());
            options.AddConsumer(busConsumerType.MakeGenericType(task));
        }
    }

    private static void AddBroker(TaskQueueConfiguration configuration, IBusRegistrationConfigurator options)
    {
        switch (configuration.BrokerImplementation)
        {
            case MessageBrokerImplementation.InMemory:
                AddInMemoryBroker(options);
                break;
            case MessageBrokerImplementation.RabbitMq:
                AddRabbitMqBroker(configuration, options);
                break;
            default:
                throw new NotImplementedException("No broker implementation found");
        }
    }

    private static void AddInMemoryBroker(IBusRegistrationConfigurator options)
    {
        options.UsingInMemory((context, cfg) =>
        {
            cfg.ConfigureEndpoints(context);
            cfg.UseNewtonsoftJsonSerializer();
        });
    }

    private static void AddRabbitMqBroker(TaskQueueConfiguration configuration, IBusRegistrationConfigurator options)
    {
        if (configuration.RabbitMqSettings is null)
        {
            throw new Exception("Could not resolve RabbitMQ settings");
        }

        options.UsingRabbitMq((context, cfg) =>
        {
            cfg.Message<TaskState>(x => x.SetEntityName("TaskStateExchange"));
            cfg.Publish<TaskState>(configurator =>
            {
                configurator.ExchangeType = "fanout";
            });
            cfg.ReceiveEndpoint($"{configuration.ConsumerSettings!.ServerName}-task-state", configurator =>
            {
                configurator.ConfigureConsumeTopology = false;
                configurator.Bind("TaskStateExchange");
                configurator.Consumer(context, (IConsumerConfigurator<TaskStateConsumer> _) =>
                {

                });
            });

            cfg.Message<CancellationRequest>(x => x.SetEntityName("TaskCancellationRequestExchange"));
            cfg.Publish<CancellationRequest>(configurator =>
            {
                configurator.ExchangeType = "fanout";
            });
            cfg.ReceiveEndpoint($"{configuration.ConsumerSettings!.ServerName}-cancellation-request", configurator => {
            {
                configurator.ConfigureConsumeTopology = false;
                configurator.Bind("TaskCancellationRequestExchange");
                configurator.Consumer(context, (IConsumerConfigurator<CancellationRequestConsumer> _) =>
                {
                    
                });
            }});
            cfg.ConfigureEndpoints(context);
            cfg.UseNewtonsoftJsonSerializer();

            var rabbitMqSettings = configuration.RabbitMqSettings;
            cfg.Host(rabbitMqSettings.Host, rabbitMqSettings.Port,
                rabbitMqSettings.VirtualHost,
                configurator =>
                {
                    configurator.Username(rabbitMqSettings.Username);
                    configurator.Password(rabbitMqSettings.Password);
                });
        });
    }
}