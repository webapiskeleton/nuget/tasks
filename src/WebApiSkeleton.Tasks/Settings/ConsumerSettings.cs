﻿using Newtonsoft.Json;

namespace WebApiSkeleton.Tasks.Settings;

public sealed class ConsumerSettings
{
    /// <summary>
    /// Unique server name used to identify it
    /// </summary>
    public required string ServerName { get; set; }

    /// <summary>
    /// <see cref="Newtonsoft.Json.JsonSerializerSettings"/> that are used in application.
    /// Must have <see cref="Newtonsoft.Json.JsonSerializerSettings.TypeNameHandling"/>
    /// set to <see cref="TypeNameHandling.All"/>
    /// </summary>
    public JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
    {
        TypeNameHandling = TypeNameHandling.All
    };
}