﻿namespace WebApiSkeleton.Tasks.Settings;

public sealed class TaskStorageSettings
{
    public required string TaskQueueTableName { get; set; } = null!;
    public required string FinishedTasksTableName { get; set; } = null!;
    public required string ExecutingTasksTableName { get; set; } = null!;
    public required string DeferredTasksTableName { get; set; } = null!;
    public required string CancelledTasksTableName { get; set; } = null!;
}