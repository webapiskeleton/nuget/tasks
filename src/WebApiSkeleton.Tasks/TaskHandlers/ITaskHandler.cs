﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeleton.Tasks.TaskHandlers;

/// <summary>
/// Should be implemented to define the handler for a specific task
/// </summary>
/// <typeparam name="TTask">The task to handle</typeparam>
/// <remarks>Cannot have more than one handler for one task type</remarks>
public interface ITaskHandler<in TTask> where TTask : BaseBackgroundTask
{
    public Task Handle(TTask task);
}