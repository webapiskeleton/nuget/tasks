﻿using System.Configuration;
using System.Reflection;
using Newtonsoft.Json;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeleton.Tasks.Integrity;
using WebApiSkeleton.Tasks.Settings;

namespace WebApiSkeleton.Tasks;

public sealed class TaskQueueConfiguration
{
    internal List<Assembly> TaskAssemblies { get; } = new();

    internal MessageBrokerImplementation? BrokerImplementation { get; private set; }

    internal RabbitMqSettings? RabbitMqSettings { get; private set; }

    /// <summary>
    /// Sets the <see cref="StackExchange.Redis.IConnectionMultiplexer"/> and database number to use redis in queue.
    /// </summary>
    /// <remarks>This is the required property. Validation will fail if this is null</remarks>
    public RedisSettings? RedisSettings { get; set; }

    /// <summary>
    /// Used to set settings of the current queue Consumer 
    /// </summary>
    /// <remarks>
    /// This is the required property. Validation will fail if this is null.
    /// Server name should be unique across all other servers in cluster to prevent unpredictable behavior.
    /// <see cref="Newtonsoft.Json.JsonSerializerSettings"/> must have
    /// <see cref="JsonSerializerSettings.TypeNameHandling"/> property set to
    /// <see cref="Newtonsoft.Json.TypeNameHandling.All"/> 
    /// </remarks>
    public ConsumerSettings? ConsumerSettings { get; set; }

    /// <summary>
    /// Sets the collection names used in Redis to store task information
    /// </summary>
    public TaskStorageSettings TaskStorageSettings { get; set; } = new()
    {
        CancelledTasksTableName = "cancelled-tasks",
        DeferredTasksTableName = "deferred-tasks",
        ExecutingTasksTableName = "executing-tasks",
        FinishedTasksTableName = "finished-tasks",
        TaskQueueTableName = "task-queue"
    };

    /// <summary>
    /// Used to configure Semaphore that is used to limit the number of concurrent tasks executing.
    /// Default maximum value is 5
    /// </summary>
    public TaskSemaphoreSettings TaskSemaphoreSettings { get; set; } = new()
        { SemaphoreName = "TaskSemaphore", MaximumRequestCount = 5 };

    /// <summary>
    /// Used to configure the implementation type of <see cref="ITaskIntegrityService"/>
    /// If the given type is not inherited from <see cref="ITaskIntegrityService"/> the validation will fail
    /// </summary>
    /// <remarks>This is the required property. Validation will fail if this is null</remarks>
    public Type? TaskIntegrityImplementationType { get; set; }

    internal void ValidateConfiguration()
    {
        if (BrokerImplementation is null)
            throw new ConfigurationErrorsException("Broker implementation is not defined");

        if (BrokerImplementation is MessageBrokerImplementation.RabbitMq && RabbitMqSettings is null)
            throw new ConfigurationErrorsException("RabbitMQ settings not set");

        if (TaskIntegrityImplementationType is null ||
            !TaskIntegrityImplementationType.IsAssignableTo(typeof(ITaskIntegrityService)))
            throw new ConfigurationErrorsException("ITaskIntegrityService is not implemented or has the wrong type");

        if (RedisSettings is null)
            throw new ConfigurationErrorsException("Redis connection settings not set");

        if (ConsumerSettings is null)
            throw new ConfigurationErrorsException("Consumer settings not set");

        if (string.IsNullOrEmpty(ConsumerSettings.ServerName))
            throw new ConfigurationErrorsException("Server name not defined");

        if (ConsumerSettings!.JsonSerializerSettings.TypeNameHandling != TypeNameHandling.All)
            throw new ConfigurationErrorsException("JsonSerializerSettings must have TypeNameHandling.All");
    }

    /// <summary>
    /// Add tasks and handler implementations from assembly defined
    /// </summary>
    /// <param name="assembly">Assembly to search for</param>
    public TaskQueueConfiguration AddTasksFromAssembly(Assembly assembly)
    {
        TaskAssemblies.Add(assembly);
        return this;
    }

    /// <summary>
    /// Add tasks and handler implementations from assemblies defined
    /// </summary>
    /// <param name="assembly">Assemblies to search for</param>
    public TaskQueueConfiguration AddTasksFromAssembly(params Assembly[] assembly)
    {
        TaskAssemblies.AddRange(assembly);
        return this;
    }

    /// <summary>
    /// Use RabbitMQ message broker.
    /// </summary>
    /// <param name="settings">Settings to configure RabbitMQ</param>
    /// <exception cref="InvalidOperationException">broker implementation was already defined</exception>
    public TaskQueueConfiguration UseRabbitMQ(RabbitMqSettings settings)
    {
        if (BrokerImplementation is not null)
            throw new InvalidOperationException("Cannot use RabbitMQ broker: broker implementation already defined");

        BrokerImplementation = MessageBrokerImplementation.RabbitMq;
        RabbitMqSettings = settings;
        return this;
    }

    /// <summary>
    /// Use In-Memory message broker. Should be used for testing purposes within single server and will not work distributed way
    /// </summary>
    /// <exception cref="InvalidOperationException">broker implementation was already defined</exception>
    public TaskQueueConfiguration UseInMemoryBroker()
    {
        if (BrokerImplementation is not null)
            throw new InvalidOperationException("Cannot use in-memory broker: broker implementation already defined");

        BrokerImplementation = MessageBrokerImplementation.InMemory;
        return this;
    }
}