﻿using Newtonsoft.Json;

namespace WebApiSkeleton.Tasks.Tasks;

/// <summary>
/// Base task type that all concrete tasks should inherit from
/// </summary>
public abstract class BaseBackgroundTask
{
    public Guid Id { get; set; }

    /// <summary>
    /// Total number of restart after failure attempts left
    /// </summary>
    public virtual int AdditionalExecuteAttemptsLeft { get; set; } = TaskConstants.DefaultAdditionalExecuteAttempts;

    /// <summary>
    /// Repeated execution attempt nubmer
    /// </summary>
    public int AttemptNumber { get; set; }

    public bool IsDeferred { get; set; }

    /// <summary>
    /// Sign that contains all deferment information
    /// </summary>
    public DeferredSign? DeferredSign { get; internal set; }

    public DateTime AddedToQueueTime { get; internal set; }
    
    /// <summary>
    /// Time when tasks started executing
    /// </summary>
    public DateTime StartedTime { get; internal set; }

    [JsonIgnore] public Action<TaskState> OnStateChanged { get; internal set; } = null!;

    [JsonIgnore] public CancellationToken CancellationToken { get; internal set; }

    public bool Repeat()
    {
        AdditionalExecuteAttemptsLeft--;
        AttemptNumber++;
        return AdditionalExecuteAttemptsLeft > 0;
    }

    #region Internal

    /// <summary>
    /// Is task restored (previously deferred)
    /// </summary>
    [JsonProperty]
    internal bool IsRestored { get; set; }

    #endregion
}