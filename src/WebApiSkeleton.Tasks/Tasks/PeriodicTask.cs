namespace WebApiSkeleton.Tasks.Tasks;

/// <summary>
/// Base periodic task type that will be executed every <see cref="Period"/>
/// </summary>
public abstract class PeriodicTask : BaseBackgroundTask
{
    /// <summary>
    /// Execution period
    /// </summary>
    public abstract TimeSpan Period { get; }

    /// <summary>
    /// Maximum periodic task iterations
    /// </summary>
    public virtual int? MaximumCompletions => null;
}